# Upload.class.php 2.0.0

auteurs : Désiré Ndri et Gwenaël Frelau

Page de démo : [http://frelau.etudiant-eemi.com/perso/3A/v2/tp_upload_ndri_frelau/demo/test_upload.php](http://frelau.etudiant-eemi.com/perso/3A/v2/tp_upload_ndri_frelau/demo/test_upload.php)

## Amélioration par rapport à la v 1.0.0


La v 2.0.0 vous permez desormais de choisir un dossier de destination différent du dossier par défaut, de choisir un nom personnalisé pour votre fichier uploadé, de choisir les extentions autorisées et de redimensionner des fichier de type jpg, png et gif.


## Comment utiliser cette class ?


créer un fichier html de base comprenant au minimum les champs suivant:
```html
<form id="formu" name="form_post" method="post" action="upload_file.php" enctype="multipart/form-data">	
	<input  type="file" name="ch_file" class="form-control" />
	<input  type="submit"  value="envoyer fichier" class="btn btn-default"/>
</form>
```

Ensuite créer un fichier nommé upload_file.php, de la sorte:
```php
include_once ('Upload.class.php');

try
{
    $name_form_input = 'ch_file'; //indiquer ici le name du champ input utilisé pour le fichier

	$file = new Upload($name_form_input, $path, $filename, $fileExtention, $newWidth, $newHeight);
}
catch (Exception $e)
{
	echo $e ->getMessage();
}
```

par défaut il vous faut créer un dossier dans le même répertoire que le fichier upload_file.php et le nommer uploadFile.
Veillez à ce que ce dossier soit accessible en lecture et en écriture pour tous les utilisateurs.

## Les options paramètrables


### Choix du chemin de destination
Par défaut chaque fichier uploadé se retrouvera dans le dossier uploadFile, que vous aveez créé précédement. Vous pouvez aussi choisir un autre dossier de destination, pour cela il vous suffit de rajouter dans le fichier html de base un champs input, de la sorte:
```html
<input  type="text" name="path" class="form-control"/>
```
Et dans le fichier upload_file.php, le code suivant, dans le try, avant l'intenciation de la class :
```php
$path = $_POST['path'];
```

### Choix du nom du fichier
Par défaut le fichier uploadé aura un nom issu d'une suite de caractères choisis au hasard, Il est possible de définir un nom personnalisé pour chaque fichié uploadé, il suffit d'ajouter dans le HTML un champs input :
```html
<input  type="text" name="filename" class="form-control"/>
```

Et dans le fichier upload_file.php, le code suivant, dans le try, avant l'intenciation de la class :
```php
$filename = $_POST['filename']; 
```
### Choix des extentions
Par défaut la class prend en compte les extentions suivantes:
'jpg' , 'jpeg', 'gif', 'png', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx'
Il est possible de paramètrer les extentions accèptées, pour cela il vous suffit de rajout un champs input dans le fichier html de base, les extentions inscrits dans ce champs devront être séparrées par une virgule ou un espace :
```html
<input  type="text" name="ext" class="form-control"/>
```

Et dans le fichier upload_file.php, le code suivant, dans le try, avant l'intenciation de la class :
```php
$fileExtention = $_POST['ext'];
```

### Redimentionner les images
Pour pouvoir donner une taille particuliere en px à vos images de type jpg, png et gif, il vous suffit d'ajouter deux champs input dans le fichier html (voir si dessous). c'est champs devront contenir la taille de la largeur et de la hauteur désirées, c'est champs ne doivent contenir que des nombres, inutile d'indiquer 'px' après ces derniers.
```html
<input  type="text" name="width" class="form-control"/>
<input  type="text" name="height" class="form-control"/>
```

Et dans le fichier upload_file.php, le code suivant, dans le try, avant l'intenciation de la class :
```php
$newWidth = $_POST['width'];
$newHeight = $_POST['height'];
```

## Méthodes


Vous pouvez appeller les méthodes afficherPath() et afficherName() qui retournent respectivement:
- le chemin du dossier où se trouve le fichier uploadé
- le nouveau nom du fichier uploadé