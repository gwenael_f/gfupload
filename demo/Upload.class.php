<?php

class Upload
{
    /**
    * Attributs
    **/

    private static $_extention;
    private static $_form_name_input;
    private static $_path;
    private static $_url;
    private static $_fileName;
    private static $_size;
    private static $_valid_extention;
    private static $_width;
    private static $_height;
    private static $_newWidth;
    private static $_newHeight;
    
    public static function _choseFileExtention($fileExtention)
    {
        if($fileExtention != "")
        {
            self::$_valid_extention = str_word_count($fileExtention, 1);
        }
        else
        {
            self::$_valid_extention = array('jpg' , 'jpeg', 'gif', 'png', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx' );
        }
    }

    public static function _directoryPath($path)
    {
        if($path != '')
        {
            self::$_path = $path;
        } 
        else
        {
            self::$_path = __DIR__."/uploadFile";
        }
    }

    public static function _checkExtention()
    {
        $extention_fichier = strtolower( substr( strrchr( $_FILES[self::$_form_name_input]['name'], '.') ,1) );
        
        if (in_array($extention_fichier, self::$_valid_extention)) {
            
            if($extention_fichier === "jpg")
            {
                self::$_extention = "jpeg";
            }
            else
            {
                self::$_extention = $extention_fichier;
            }
            
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function _moveFile()
    {

        if(move_uploaded_file($_FILES[self::$_form_name_input]['tmp_name'],self::$_url))
        {
             return true;
        }
        else
        {
            return false;
        }
    }

    public static function _choseFileName($filename)
    {
        if($filename != "")
        {
            self::$_fileName = $filename;
        }
        else
        {
            self::$_fileName = md5(uniqid(mt_rand()));
        }
    }

    public static function _checkImage()
    {
        $imageExt = array("jpeg", "png", "gif");

        if(in_array(self::$_extention, $imageExt))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function _resizeImage()
    {
        $fonctionImageCreateFrom = "imagecreatefrom".self::$_extention;
        $fonctionImageExt = 'image'.self::$_extention;
        $image = $fonctionImageCreateFrom(self::$_url);
        $width = imagesx($image);
        $height = imagesy($image);
        
        if ($width>$height)
        {
            $new_width = self::$_width;
            $tmp_height = ($new_width * $height)/$width;
            self::$_newWidth = $new_width;
            self::$_newHeight = $tmp_height;
        }
        else
        {
            $new_height = self::$_height;
            $tmp_width = ($new_height * $width)/$height;
            self::$_newWidth = $tmp_width;
            self::$_newHeight = $new_height;

        }
        
        $thumb = imagecreatetruecolor(self::$_newWidth, self::$_height);

        if(self::$_extention == "png")
        {
            imagealphablending($thumb, false);
            imagesavealpha($thumb, true);
        }

        imagecopyresampled($thumb, $image, 0,0,0,0, self::$_newWidth, self::$_newHeight, $width, $height);
        $fonctionImageExt($thumb, self::$_url);
        //chmod(self::$_url,0777);  //gerer les droits pour la photo
        if(imagedestroy($image))
        {
            return true;
        }
    }

    //*********************************************************************************
    //*********************************************************************************
    //      CONSTRUCTOR
    //*********************************************************************************
    //*********************************************************************************

    public function __construct($name_form_input, $path, $filename, $fileExtention, $newWidth, $newHeight)
    {
        
        self::_choseFileExtention($fileExtention);
        self::$_form_name_input = $name_form_input;
        self::$_size = getimagesize($_FILES[self::$_form_name_input]['tmp_name']);
        self::$_width = $newWidth;
        self::$_height = $newHeight;
        self::_directoryPath($path);
        self::$_path;
        self::_choseFileName($filename);

        if(!(self::_checkExtention()))
        {
            throw new InvalidArgumentException("extention non prise en charge");
        }
        else
        {
            self::$_url = self::$_path . '/' . self::$_fileName . '.' . self::$_extention;

            if(!(self::_moveFile()))
            {
                throw new InvalidArgumentException("l'upload à rencontré un probleme");
            }
            else
            { 
                if((self::_checkImage()) && (self::$_width != "") && (self::$_height != ""))
                {
                    if(!(self::_resizeImage()))
                    {
                        throw new InvalidArgumentException("Le resize de l'imgae a rencontrer un probleme");
                    }
                }
            }
        }   
    }

    public function afficherPath()
    {
        return self::$_path;
    }

    public function afficherName()
    {
        return self::$_fileName;
    }
}
